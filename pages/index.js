import Head from 'next/head'

import Slider from '../components/slider';

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Home page</title>
      </Head>

      <h1>Home.</h1>

      <Slider />
    </div>
  )
}
