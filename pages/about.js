import Head from 'next/head'

import Slider from '../components/slider';

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>About page</title>
      </Head>

      <h1>About.</h1>
    </div>
  )
}
