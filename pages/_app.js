import Nav from '../components/nav';

import '../styles/index.scss';

function App({ Component, pageProps }) {
  return (
    <>
      <Nav />

      <Component {...pageProps} />
    </>
  )
}

export default App;