class Slider {
  constructor() {
    this.dom = {};
    this.dom.el = document.querySelector('.js-slider');
    this.dom.slides = this.dom.el.querySelectorAll('.js-slide');
    this.dom.controls = this.dom.el.querySelectorAll('.js-button');

    this.state = {
      dir: undefined
    };
  }

  animate() {
    this.state.dir === '<' && console.log('< prev');
    this.state.dir === '>' && console.log('> next');
  }

  handleClick = (e) => {
    this.state.dir = e.target.dataset.dir;

    this.animate();
  }

  addListeners() {
    [...this.dom.controls].forEach((button) => button.addEventListener('click', this.handleClick));
  }

  removeListeners() {
    [...this.dom.controls].forEach((button) => button.removeEventListener('click', this.handleClick));
  }

  init() {
    console.log('init');
    this.addListeners();
  }

  destroy() {
    console.log('destroy');
    this.removeListeners();
  }
}

export default Slider;