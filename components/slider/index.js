import { useEffect } from 'react';

import Instance from './Slider';

function Slider() {
  useEffect(() => {
    const instance = new Instance();
    instance.init();

    return () => {
      instance.destroy();
    };
  }, []);

  return (
    <div className="slider js-slider">
      <div className="slider__slides">
        <img
          src="https://images.unsplash.com/photo-1557543878-042c9857f39e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"
          alt="landscape image"
          className="slider__slide js-slide"
        />
        <img
          src="https://images.unsplash.com/photo-1510939859912-0e7e5904c400?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"
          alt="landscape image"
          className="slider__slide js-slide"
        />
        <img
          src="https://images.unsplash.com/photo-1540028317582-ab90fe7c343f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"
          alt="landscape image"
          className="slider__slide js-slide"
        />
      </div>

      <div className="slider__controls">
      <button
          type="button"
          className="js-button"
          data-dir="<"
        >
          prev
        </button>

        <button
          type="button"
          className="js-button"
          data-dir=">"
        >
          next
        </button>
      </div>
    </div>
  )
}

export default Slider;